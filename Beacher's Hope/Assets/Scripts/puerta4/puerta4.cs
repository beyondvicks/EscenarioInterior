using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puerta4 : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("puerta_4");
    }
    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("puerta_4cerrar");
    }
}
