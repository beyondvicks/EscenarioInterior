using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cofreJuanMason : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("cofreJuanMarson");
    }
    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("cerrarcofreJuanMason");
    }
}
