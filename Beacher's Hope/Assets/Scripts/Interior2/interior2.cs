using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class interior2 : MonoBehaviour
{
    public Animator Puerta;

    private void OnTriggerEnter(Collider other)
    {
        Puerta.Play("puertaInterior_2");
    }
    private void OnTriggerExit(Collider other)
    {
        Puerta.Play("puertaInterior_2cerrar");
    }
}
